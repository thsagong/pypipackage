import setuptools

setuptools.setup(
    name="mypypipackage",
    version="0.0.3",
    author="TH Sagong",
    author_email="thsagong@wisenut.co.kr",
    description="A small example package",
    packages=setuptools.find_packages(exclude=['src']),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)